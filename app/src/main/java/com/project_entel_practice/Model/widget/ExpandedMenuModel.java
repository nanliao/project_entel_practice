package com.project_entel_practice.Model.widget;

public class ExpandedMenuModel {
    String iconName = "";
    int iconImg = -1; // menu icon resource id
    boolean isExpanded = false;

    public String getIconName() {
        return iconName;
    }

    public void setIconName(String iconName) {
        this.iconName = iconName;
    }

    public int getIconImg() {
        return iconImg;
    }

    public void setIconImg(int iconImg) {
        this.iconImg = iconImg;
    }

    public boolean isExpanded() {
        return isExpanded;
    }

    public void setExpanded(boolean expanded) {
        isExpanded = expanded;
    }
}
