package com.project_entel_practice.View.activities;

import android.graphics.Color;
import android.support.constraint.ConstraintLayout;
import android.support.design.widget.NavigationView;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.Gravity;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.animation.AlphaAnimation;
import android.view.animation.TranslateAnimation;
import android.widget.ExpandableListView;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.project_entel_practice.Model.widget.ExpandedMenuModel;
import com.project_entel_practice.R;
import com.project_entel_practice.View.adapters.ExpandableListAdapter;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

public class HomeActivity extends AppCompatActivity {

    int count;
    boolean isExpanded = false;


    DrawerLayout mDrawerLayout;
    ExpandableListAdapter mMenuAdapter;
    ExpandableListView expandableList;
    List<ExpandedMenuModel> listDataHeader;
    HashMap<ExpandedMenuModel, List<String>> listDataChild;
    ImageView iv_close_drawer;
    ConstraintLayout cl_menu_main, cl_saldos_main, cl_recargas_main, cl_beneficios_main, cl_content_for_fragments_main;
    LinearLayout llayout_main, lLayaout_flag_bottom_sheet, lL_rechargue_bottom_sheet, lL_bag_bottom_sheet, lL_history_bottom_sheet;

    ArrayList<ConstraintLayout> bottomBarButtons;

    @Override
    protected void onCreate(Bundle savedInstanceState) {

        bottomBarButtons = new ArrayList<>();

        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_home);
        final ActionBar ab = getSupportActionBar();


        cl_content_for_fragments_main = (ConstraintLayout) findViewById(R.id.id_constraint_for_fragments);
        cl_menu_main = (ConstraintLayout) findViewById(R.id.id_constraintLayout_menu_main);
        cl_saldos_main = (ConstraintLayout) findViewById(R.id.id_constraintLayout_saldos_main);
        cl_recargas_main = (ConstraintLayout) findViewById(R.id.id_constraintLayout_recargas_main);
        cl_beneficios_main = (ConstraintLayout) findViewById(R.id.id_constraintLayout_beneficios_main);

        llayout_main = (LinearLayout) findViewById(R.id.id_main_linearLayout);
        lLayaout_flag_bottom_sheet = (LinearLayout) findViewById(R.id.id_flag_bottom_sheet);

        lL_rechargue_bottom_sheet = (LinearLayout) findViewById(R.id.lL_rechargue_bottom_sheet);
        lL_bag_bottom_sheet = (LinearLayout) findViewById(R.id.lL_bag_bottom_sheet);
        lL_history_bottom_sheet = (LinearLayout) findViewById(R.id.lL_history_bottom_sheet);


        bottomBarButtons.add(cl_menu_main);
        bottomBarButtons.add(cl_saldos_main);
        bottomBarButtons.add(cl_beneficios_main);
        bottomBarButtons.add(cl_recargas_main);

        for (int i = 0; i < bottomBarButtons.size(); i++) {
            changueAlpha(bottomBarButtons.get(i), 0.5F);
        }


        iv_close_drawer = (ImageView) findViewById(R.id.id_close_drawer);
        mDrawerLayout = (DrawerLayout) findViewById(R.id.drawer_layout);
        expandableList = (ExpandableListView) findViewById(R.id.navigationmenu);
        NavigationView navigationView = (NavigationView) findViewById(R.id.nav_view);

        lL_rechargue_bottom_sheet.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                if (isExpanded) {

                    Toast.makeText(getApplicationContext(), "This is my Toast message!",
                            Toast.LENGTH_LONG).show();
                }
            }
        });

        cl_content_for_fragments_main.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {


                if (isExpanded) {
                    slideDown(lLayaout_flag_bottom_sheet);
                    cl_content_for_fragments_main.setBackgroundColor(Color.WHITE);
                    isExpanded = false;
                }

            }
        });


        cl_menu_main.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                focusButton(bottomBarButtons, cl_menu_main);

                if (isExpanded) {
                    slideDown(lLayaout_flag_bottom_sheet);
                    cl_content_for_fragments_main.setBackgroundColor(Color.WHITE);
                    isExpanded = false;
                }

                openDrawer();

            }
        });

        cl_beneficios_main.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                focusButton(bottomBarButtons, cl_beneficios_main);

                if (isExpanded) {
                    slideDown(lLayaout_flag_bottom_sheet);
                    cl_content_for_fragments_main.setBackgroundColor(Color.WHITE);
                    isExpanded = false;
                }

            }
        });

        cl_saldos_main.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                focusButton(bottomBarButtons, cl_saldos_main);

                if (isExpanded) {
                    slideDown(lLayaout_flag_bottom_sheet);
                    cl_content_for_fragments_main.setBackgroundColor(Color.WHITE);
                    isExpanded = false;
                }
            }
        });


        cl_recargas_main.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                focusButton(bottomBarButtons, cl_recargas_main);

                if (isExpanded) {
                    slideDown(lLayaout_flag_bottom_sheet);
                    cl_content_for_fragments_main.setBackgroundColor(Color.WHITE);
                    isExpanded = false;
                } else {
                    slideUp(lLayaout_flag_bottom_sheet);
                    cl_content_for_fragments_main.setBackgroundColor(Color.parseColor("#50161616"));
                    isExpanded = true;
                }

                //isExpanded = !isExpanded;
            }
        });


        iv_close_drawer.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View view) {

                closeDrawer();
            }
        });


        if (navigationView != null) {
            setupDrawerContent(navigationView);
        }

        prepareListData();
        mMenuAdapter = new ExpandableListAdapter(this, listDataHeader, listDataChild, expandableList);

        // setting list adapter

        expandableList.setAdapter(mMenuAdapter);


        expandableList.setOnChildClickListener(new ExpandableListView.OnChildClickListener() {
            @Override
            public boolean onChildClick(ExpandableListView expandableListView, View view, int i, int i1, long l) {
                //Log.d("DEBUG", "submenu item clicked");

                return false;
            }
        });
        expandableList.setOnGroupClickListener(new ExpandableListView.OnGroupClickListener() {
            @Override
            public boolean onGroupClick(ExpandableListView expandableListView, View view, int groupPosition, long l) {

                expandableListView.setDividerHeight(2);

                //count = expandableListView.getExpandableListAdapter().getChildrenCount(groupPosition);

                TextView tv_background = (TextView) view.findViewById(R.id.bottom_background_list_header);
                ImageView groupIndicator = (ImageView) view.findViewById(R.id.id_image_header);


                return false;
            }
        });

        expandableList.setIndicatorBounds(expandableList.getWidth() + 50, expandableList.getWidth());


    }

    public void slideUp(View view) {
        view.setVisibility(View.VISIBLE);
        TranslateAnimation animate = new TranslateAnimation(
                0,                 // fromXDelta
                0,                 // toXDelta
                view.getHeight(),  // fromYDelta
                0);                // toYDelta
        animate.setDuration(250);
        animate.setFillAfter(true);
        view.startAnimation(animate);
    }

    public void slideDown(View view) {

        TranslateAnimation animate = new TranslateAnimation(
                0,                 // fromXDelta
                0,                 // toXDelta
                0,                 // fromYDelta
                view.getHeight()); // toYDelta
        animate.setDuration(500);
        animate.setFillAfter(true);
        view.startAnimation(animate);

    }

    private void prepareListData() {
        listDataHeader = new ArrayList<ExpandedMenuModel>();
        listDataChild = new HashMap<ExpandedMenuModel, List<String>>();

        ExpandedMenuModel item1 = new ExpandedMenuModel();
        item1.setIconName("Mi línea");
        item1.setIconImg(R.drawable.ic_icon_line);
        // Adding data header
        listDataHeader.add(item1);

        ExpandedMenuModel item2 = new ExpandedMenuModel();
        item2.setIconName("Recargas y bolsas");
        item2.setIconImg(R.drawable.ic_icon_smartphone);
        listDataHeader.add(item2);

        ExpandedMenuModel item3 = new ExpandedMenuModel();
        item3.setIconName("Beneficios cine");
        item3.setIconImg(R.drawable.ic_icon_cinema);
        listDataHeader.add(item3);

        ExpandedMenuModel item4 = new ExpandedMenuModel();
        item4.setIconName("Tiendas");
        item4.setIconImg(R.drawable.ic_icon_shop);
        listDataHeader.add(item4);

        ExpandedMenuModel item5 = new ExpandedMenuModel();
        item5.setIconName("Cobertura");
        item5.setIconImg(R.drawable.ic_icon_wifi);
        listDataHeader.add(item5);

        ExpandedMenuModel item6 = new ExpandedMenuModel();
        item6.setIconName("Ayuda y soporte");
        item6.setIconImg(R.drawable.ic_icon_help);
        listDataHeader.add(item6);

        // Adding child data
        List<String> heading1 = new ArrayList<String>();
        heading1.add("Mis saldos");
        heading1.add("Detalle de plan");


        List<String> heading2 = new ArrayList<String>();
        heading2.add("Recargas");
        heading2.add("Bolsas");
        heading2.add("Historial de compras");

        List<String> heading3 = new ArrayList<String>();
        List<String> heading4 = new ArrayList<String>();
        heading4.add("Ubícanos");
        heading4.add("Ticket en Tienda");


        List<String> heading5 = new ArrayList<String>();


        List<String> heading6 = new ArrayList<String>();
        heading6.add("Contáctanos");
        heading6.add("Sugerencias");


        listDataChild.put(listDataHeader.get(0), heading1);// Header, Child data
        listDataChild.put(listDataHeader.get(1), heading2);
        listDataChild.put(listDataHeader.get(2), heading3);// Header, Child data
        listDataChild.put(listDataHeader.get(3), heading4);
        listDataChild.put(listDataHeader.get(4), heading5);// Header, Child data
        listDataChild.put(listDataHeader.get(5), heading6);

    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                mDrawerLayout.openDrawer(GravityCompat.START);
                return true;
        }

        return super.onOptionsItemSelected(item);
    }

    private void setupDrawerContent(NavigationView navigationView) {
        //revision: this don't works, use setOnChildClickListener() and setOnGroupClickListener() above instead
        navigationView.setNavigationItemSelectedListener(
                new NavigationView.OnNavigationItemSelectedListener() {
                    @Override
                    public boolean onNavigationItemSelected(MenuItem menuItem) {
                        menuItem.setChecked(false);
                        mDrawerLayout.closeDrawers();
                        return true;
                    }
                });
    }

    public void closeDrawer() {
        this.mDrawerLayout.closeDrawer(Gravity.LEFT);
    }

    public void openDrawer() {
        this.mDrawerLayout.openDrawer(Gravity.START);
    }

    public void changueAlpha(ConstraintLayout objConstraintLayout, float numberDouble) {
        AlphaAnimation objAlpha = new AlphaAnimation(numberDouble, numberDouble);
        objAlpha.setDuration(0);
        objAlpha.setFillAfter(true);
        objConstraintLayout.startAnimation(objAlpha);
    }

    public void focusButton(ArrayList<ConstraintLayout> listButton, ConstraintLayout objConstraintLayout) {


        for (int i = 0; i < listButton.size(); i++) {

            if (listButton.get(i) == objConstraintLayout) {
                changueAlpha(listButton.get(i), 1);
            } else {
                changueAlpha(listButton.get(i), 0.5F);
            }

        }
    }

}
