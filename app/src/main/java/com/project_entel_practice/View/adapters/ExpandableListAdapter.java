package com.project_entel_practice.View.adapters;

import android.content.Context;
import android.content.res.Resources;
import android.graphics.Color;
import android.graphics.Typeface;
import android.support.constraint.ConstraintLayout;
import android.support.constraint.ConstraintSet;
import android.support.constraint.Guideline;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseExpandableListAdapter;
import android.widget.ExpandableListView;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import java.util.List;
import java.util.HashMap;

import com.project_entel_practice.Model.widget.ExpandedMenuModel;
import com.project_entel_practice.R;


public class ExpandableListAdapter extends BaseExpandableListAdapter {

    private Context mContext;
    private List<ExpandedMenuModel> mListDataHeader; // header titles


    // child data in format of header title, child title
    private HashMap<ExpandedMenuModel, List<String>> mListDataChild;
    ExpandableListView expandList;

    public ExpandableListAdapter(Context context, List<ExpandedMenuModel> listDataHeader, HashMap<ExpandedMenuModel, List<String>> listChildData, ExpandableListView mView) {
        this.mContext = context;
        this.mListDataHeader = listDataHeader;
        this.mListDataChild = listChildData;
        this.expandList = mView;
    }

    @Override
    public int getGroupCount() {
        int i = mListDataHeader.size();
        Log.d("GROUPCOUNT", String.valueOf(i));
        return this.mListDataHeader.size();
    }

    @Override
    public int getChildrenCount(int groupPosition) {
        int childCount = 0;
        if (groupPosition != 2) {
            childCount = this.mListDataChild.get(this.mListDataHeader.get(groupPosition))
                    .size();
        }

        return childCount;
    }

    @Override
    public Object getGroup(int groupPosition) {
        return this.mListDataHeader.get(groupPosition);
    }

    @Override
    public Object getChild(int groupPosition, int childPosition) {
        Log.d("CHILD", mListDataChild.get(this.mListDataHeader.get(groupPosition))
                .get(childPosition).toString());
        return this.mListDataChild.get(this.mListDataHeader.get(groupPosition))
                .get(childPosition);
    }

    @Override
    public long getGroupId(int groupPosition) {
        return groupPosition;
    }

    @Override
    public long getChildId(int groupPosition, int childPosition) {
        return childPosition;
    }

    @Override
    public boolean hasStableIds() {
        return false;
    }

    @Override
    public View getGroupView(int groupPosition, boolean isExpanded, View convertView, ViewGroup parent) {

        ExpandedMenuModel headerTitle = (ExpandedMenuModel) getGroup(groupPosition);
        if (convertView == null) {
            LayoutInflater infalInflater = (LayoutInflater) this.mContext
                    .getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            convertView = infalInflater.inflate(R.layout.list_header, null);
        }

        //count = getChildrenCount(groupPosition);

        TextView lblListHeader = (TextView) convertView.findViewById(R.id.submenu);
        ImageView objImageView = (ImageView) convertView.findViewById(R.id.id_image_header);
        ImageView headerIcon = (ImageView) convertView.findViewById(R.id.iconimage);

        TextView tv_background = (TextView) convertView.findViewById(R.id.bottom_background_list_header);
        ImageView groupIndicator = (ImageView) convertView.findViewById(R.id.id_image_header);

        //lblListHeader.setTypeface(null, Typeface.BOLD);
        lblListHeader.setText(headerTitle.getIconName());

        headerIcon.setImageResource(headerTitle.getIconImg());
        //objImageView.setImageResource(R.drawable.ic_keyboard_arrow_down_black_24dp);

        if (this.getChildrenCount(groupPosition) > 0) {
            //objImageView.setVisibility(View.INVISIBLE);
            if (isExpanded) {
                objImageView.setImageResource(R.drawable.ic_keyboard_arrow_up_black_24dp);
                tv_background.setBackgroundResource(R.color.color_orange_custom);

            } else {
                objImageView.setImageResource(R.drawable.ic_keyboard_arrow_down_black_24dp);
                tv_background.setBackgroundResource(R.color.color_background_child);
            }


        } else
            objImageView.setImageResource(android.R.color.transparent);

        if (this.getChildrenCount(groupPosition) == 0) {
            tv_background.setBackgroundResource(R.color.color_background_child);

        }


        return convertView;
    }


    @Override
    public View getChildView(int groupPosition, int childPosition, boolean isLastChild, View convertView, ViewGroup parent) {


        final String childText = (String) getChild(groupPosition, childPosition);

        if (convertView == null) {
            LayoutInflater infalInflater = (LayoutInflater) this.mContext
                    .getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            convertView = infalInflater.inflate(R.layout.list_submenu, null);


        }

        //if(getChildrenCount(groupPosition)>0){
        TextView txtListChild = (TextView) convertView.findViewById(R.id.submenu);
        txtListChild.setGravity(View.TEXT_ALIGNMENT_TEXT_START);
        // txtListChild.setPadding(100,0,0,0);
        txtListChild.setText(childText);
        // }


        return convertView;
    }

    @Override
    public boolean isChildSelectable(int groupPosition, int childPosition) {
        return true;
    }


}